package com.example.revolut.list

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.revolut.R
import com.example.revolut.list.presentation.ListViewModel
import com.example.revolut.list.presentation.ListOfCurrenciesScreenUpdate
import com.example.revolut.list.presentation.item.ItemViewModel
import com.example.revolut.list.presentation.view.ListOfCurrenciesFragment
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@RunWith(AndroidJUnit4::class)
class ListOfCurrencyItemsFragmentTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()
    private val updateEvent = MutableLiveData<ListOfCurrenciesScreenUpdate>()
    private val mockListViewModel = mock<ListViewModel> {
        on { listOfCurrenciesScreenUpdateEvent } doReturn updateEvent
    }

    @Test
    @Config(sdk = [28])
    fun `No errors when recreated`() {
        getScenario().recreate()
    }

    @Test
    @Config(sdk = [28])
    fun `Set items to adapter`() {
        getScenario().onFragment {
            updateEvent.value = ListOfCurrenciesScreenUpdate(
                listOf(
                    ItemViewModel("Title1", "", "", mock()),
                    ItemViewModel("Title2", "", "", mock())
                ), false
            )
            //verify
            assertEquals(2, it.view!!.findViewById<RecyclerView>(R.id.recycler_view).adapter!!.itemCount)
        }
    }

    @Test
    @Config(sdk = [28])
    fun `Scroll to top`() {
        getScenario().onFragment {
            val list = mutableListOf<ItemViewModel>()
            for (i in 0..enoughItemsToScrollOutOfScreen) list.add(ItemViewModel("Title1", "", "", mock()))
            updateEvent.value = ListOfCurrenciesScreenUpdate(list, false)
            val recyclerView = it.view!!.findViewById<RecyclerView>(R.id.recycler_view)
            recyclerView.scrollToPosition(list.size - 1)
            //act
            updateEvent.value = ListOfCurrenciesScreenUpdate(list, true)
            //verify
            assertFalse(recyclerView.canScrollVertically(-1))
        }
    }

    private fun getScenario(): FragmentScenario<ListOfCurrenciesFragment> {
        return launchFragmentInContainer {
            ListOfCurrenciesFragment().also {
                it.injector.inject = {
                    it.viewModel = mockListViewModel
                }
            }
        }
    }
}

private const val enoughItemsToScrollOutOfScreen = 30