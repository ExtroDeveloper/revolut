package com.example.revolut.list.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.revolut.list.domain.CurrencyItems
import com.example.revolut.list.domain.CurrenciesScreenEntity
import com.example.revolut.list.domain.CurrenciesScreenInteractor
import com.example.revolut.list.presentation.item.ItemViewModel
import com.nhaarman.mockitokotlin2.*
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.disposables.SerialDisposable
import org.junit.Assert.assertSame
import org.junit.Rule
import org.junit.Test

class ListViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()
    private val mockInteractor = mock<CurrenciesScreenInteractor>()
    private val mapper = mock<Mapper>()
    private val mockSerialDisposable = mock<SerialDisposable>()
    private val entity = mock<CurrenciesScreenEntity>()
    private val dataFromEntity = CurrencyItems()
    private val viewModel = ListViewModel(mockInteractor, mapper = mapper, disposable = mockSerialDisposable)
    private val name = "aa"
    private val value = 100f

    @Test
    fun subscribeToDataOnStart() {
        val disposable = mock<Disposable>()
        whenever(mockInteractor.listenForData(any())).doReturn(disposable)

        val listViewModel = ListViewModel(mockInteractor, mock(), disposable = mockSerialDisposable)

        verify(mockInteractor).listenForData(listViewModel)
        verify(mockSerialDisposable).set(disposable)
    }

    @Test
    fun show() {
        val mapped = listOf<ItemViewModel>(mock(), mock())
        whenever(entity.items).doReturn(dataFromEntity)
        whenever(mapper.map(dataFromEntity, viewModel)).doReturn(mapped)

        viewModel.show(entity)
        viewModel.listOfCurrenciesScreenUpdateEvent.observeForever {  }

        assertSame(mapped, viewModel.listOfCurrenciesScreenUpdateEvent.value!!.items)
    }

    @Test
    fun setAmount() {
        val disposable = mock<Disposable>()
        whenever(mockInteractor.update(any(), any(), any())).doReturn(disposable)

        viewModel.setAmount(name, value)

        verify(mockInteractor).update(name, value, viewModel)
        verify(mockSerialDisposable).set(disposable)
    }

    @Test
    fun onClick() {
        val disposable = mock<Disposable>()
        whenever(mockInteractor.putOnTop(name, viewModel)).doReturn(disposable)

        viewModel.onClick(name)

        verify(mockInteractor).putOnTop(name, viewModel)
        verify(mockSerialDisposable).set(disposable)
    }

    @Test
    fun onCleared() {
        viewModel.onCleared()

        verify(mockSerialDisposable).dispose()
    }
}