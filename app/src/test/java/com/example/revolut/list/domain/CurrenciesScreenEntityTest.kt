package com.example.revolut.list.domain

import com.example.revolut.list.data.Rate
import com.example.revolut.list.data.Rates
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class CurrenciesScreenEntityTest {

    private val entity = CurrenciesScreenEntity()
    private val items = listOf(
        Rate("aa", 2f),
        Rate("bb", 3f)
    )

    @Test
    fun `Set items first time`() {
        entity.setRates(Rates("cc", items))

        assertEquals(CurrencyItem("cc", 1f, 0f), entity.items.currencyItems[0])
        assertEquals(CurrencyItem("aa", 2f, 0f), entity.items.currencyItems[1])
        assertEquals(CurrencyItem("bb", 3f, 0f), entity.items.currencyItems[2])
    }

    @Test
    fun `Set items after update value`() {
        entity.setRates(Rates("cc", items))

        entity.update("aa", 100f)

        assertEquals(CurrencyItem("cc", 1f, 50f), entity.items.currencyItems[0])
        assertEquals(CurrencyItem("aa", 2f, 100f), entity.items.currencyItems[1])
        assertEquals(CurrencyItem("bb", 3f, 150f), entity.items.currencyItems[2])
    }

    @Test
    fun `Given rates not set, when update, do nothing`() {
        entity.update("aa", 100f)

        assertTrue(entity.items.currencyItems.isEmpty())
    }

    @Test
    fun `Given rates set, when update with currency not in list, do nothing`() {
        entity.setRates(Rates("cc", items))
        entity.update("mmm", 100f)

        assertEquals(CurrencyItem("cc", 1f, 0f), entity.items.currencyItems[0])
        assertEquals(CurrencyItem("aa", 2f, 0f), entity.items.currencyItems[1])
        assertEquals(CurrencyItem("bb", 3f, 0f), entity.items.currencyItems[2])
    }

    @Test
    fun `Given rates set and amount updated, when update one more time, keeps amount`() {
        entity.setRates(Rates("cc", items))
        entity.update("aa", 100f)
        entity.setRates(Rates("cc", items))

        assertEquals(CurrencyItem("cc", 1f, 50f), entity.items.currencyItems[0])
        assertEquals(CurrencyItem("aa", 2f, 100f), entity.items.currencyItems[1])
        assertEquals(CurrencyItem("bb", 3f, 150f), entity.items.currencyItems[2])
    }

    @Test
    fun putOnTop() {
        entity.setRates(Rates("cc", items))

        entity.putOnTop("bb")
        entity.setRates(Rates("cc", items))

        assertEquals(CurrencyItem("bb", 3f, 0f), entity.items.currencyItems[0])
        assertEquals(CurrencyItem("cc", 1f, 0f), entity.items.currencyItems[1])
        assertEquals(CurrencyItem("aa", 2f, 0f), entity.items.currencyItems[2])
    }
}