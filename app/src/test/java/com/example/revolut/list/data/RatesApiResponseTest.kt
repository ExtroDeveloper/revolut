package com.example.revolut.list.data

import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import junit.framework.Assert.assertEquals
import org.junit.Test

class RatesApiResponseTest {

    @Test
    fun deserialize() {
        val json = "{\"baseCurrency\":\"EUR\",\"rates\":{\"AUD\":1.59,\"BGN\":1.982,\"BRL\":4.259}}"

        val response = GsonBuilder()
            .enableComplexMapKeySerialization()
            .serializeNulls()
            .create()
            .fromJson<RatesApiResponse>(
                json,
                object : TypeToken<RatesApiResponse>() {}.type
            )

        assertEquals("EUR", response.baseCurrency)
        val rate = response.rates

        assertEquals(3, rate.size)
        assertEquals(1.59f, rate["AUD"])
        assertEquals(1.982f, rate["BGN"])
        assertEquals(4.259f, rate["BRL"])
    }
}