package com.example.revolut.list.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.revolut.list.domain.CurrencyItems
import com.example.revolut.list.domain.CurrencyItem
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import java.util.*

class MapperTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Test
    fun map() {
        val fullNames = mock<SubtitleProvider> {
            on { getFullName("aa") } doReturn "aa aa"
            on { getFullName("bb") } doReturn "bb bb"
        }
        val mapper = Mapper(fullNames, mock())
        val stabData = LinkedList<CurrencyItem>()
        stabData.apply {
            add(CurrencyItem("aa", 1f, 200.123f))
            add(CurrencyItem("bb", 2f, 400.54321f))
        }
        val amountListener = mock<ItemListener>()
        val result = mapper.map(CurrencyItems(stabData), amountListener)

        val itemViewModel0 = result[0]
        assertEquals("aa", itemViewModel0.title)
        assertEquals("aa aa", itemViewModel0.subTitle)
        assertEquals("200.12", itemViewModel0.amount.value)

        val itemViewModel1 = result[1]
        assertEquals("bb", itemViewModel1.title)
        assertEquals("bb bb", itemViewModel1.subTitle)
        assertEquals("400.54", itemViewModel1.amount.value)
    }
}