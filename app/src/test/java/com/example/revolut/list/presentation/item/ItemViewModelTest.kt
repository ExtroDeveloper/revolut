package com.example.revolut.list.presentation.item

import android.os.Handler
import androidx.lifecycle.MutableLiveData
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Assert.assertEquals
import org.junit.Test

class ItemViewModelTest {

    @Test
    fun `When initialized then post`() {
        val notifier = mock<ItemNotifier>()
        val initialAmount = "123"
        val amount = mock<MutableLiveData<String>>()
        val handler = mock<Handler>()
        ItemViewModel("", "", initialAmount, notifier, handler, amount)

        val captor = argumentCaptor<AmountObserver>()
        verify(handler).post(captor.capture())

        assertEquals(initialAmount, captor.firstValue.previous)
        assertEquals(amount, captor.firstValue.liveData)
        assertEquals(notifier, captor.firstValue.notifier)
    }

    @Test
    fun onClick() {
        val notifier = mock<ItemNotifier>()

        ItemViewModel("", "", "123", notifier, mock(), mock()).onClick(mock())

        verify(notifier).notifyClick()
    }
}