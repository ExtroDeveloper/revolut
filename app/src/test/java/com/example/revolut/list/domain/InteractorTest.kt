package com.example.revolut.list.domain

import com.example.revolut.list.TrampolineSchedulerRule
import com.example.revolut.list.data.Rates
import com.example.revolut.list.data.RatesRepository
import com.example.revolut.list.presentation.ListViewModel
import com.nhaarman.mockitokotlin2.*
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.TestScheduler
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.inOrder
import org.mockito.junit.MockitoJUnitRunner
import java.util.concurrent.TimeUnit.SECONDS
import java.util.concurrent.TimeUnit.MILLISECONDS

@RunWith(MockitoJUnitRunner.StrictStubs::class)
class InteractorTest {

    private val scheduler = TestScheduler()

    @get:Rule
    val rxSchedulerRule = TrampolineSchedulerRule(scheduler)
    private val entity = mock<CurrenciesScreenEntity>()
    private val repository = mock<RatesRepository>()
    private val interactor = CurrenciesScreenInteractor(entity, repository)
    private val viewModel = mock<ListViewModel>()
    private val mock0 = mock<Rates>()
    private val mock1 = mock<Rates>()
    private val mock2 = mock<Rates>()
    private val name = "name"
    private val value = 111F

    @Before
    fun setUp() {
        whenever(repository.getData())
            .doReturn(
                Observable.just(mock0)
            )
            .thenReturn(
                Observable.just(mock1)
            )
            .thenReturn(
                Observable.just(mock2)
            )
    }

    @Test
    fun listenForData() {
        interactor.listenForData(viewModel)

        checkIfShowsData()
    }

    @Test
    fun update() {
        interactor.update(name, value, viewModel)

        verify(entity).update(name, value)

        checkIfShowsData()
    }

    @Test
    fun putOnTop() {
        interactor.putOnTop(name, viewModel)

        verify(entity).putOnTop(name)

        checkIfShowsData()
    }

    private fun checkIfShowsData() {
        scheduler.advanceTimeBy(UPDATE_TIME_SEC, MILLISECONDS)

        val inOrder = inOrder(entity, viewModel)
        inOrder.verify(entity).setRates(mock0)
        inOrder.verify(viewModel).show(entity)

        scheduler.advanceTimeBy(UPDATE_TIME_SEC, SECONDS)

        inOrder.verify(entity).setRates(mock1)
        inOrder.verify(viewModel).show(entity)

        scheduler.advanceTimeBy(UPDATE_TIME_SEC, SECONDS)

        inOrder.verify(entity).setRates(mock2)
        inOrder.verify(viewModel).show(entity)

        verifyNoMoreInteractions(entity, viewModel)
    }
}