package com.example.revolut.di

import com.example.revolut.list.data.Service
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Top level DI component, creates tools for working with web storage.
 */
@Module
class AppModule {

    @Provides
    fun service(): Service {

        val gson = GsonBuilder()
            .setLenient()
            .create()

        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build().create(Service::class.java)
    }
}

private const val baseUrl = "https://hiring.revolut.codes/"