package com.example.revolut.di

import com.example.revolut.list.data.Service
import dagger.Component
import retrofit2.Retrofit

/**
 * Top level DI component, provides tools for working with web storage.
 */
@Component(modules = [AppModule::class])
interface AppComponent {

    fun provideService(): Service

}