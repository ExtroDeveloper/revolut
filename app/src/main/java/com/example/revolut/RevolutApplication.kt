package com.example.revolut

import android.app.Application
import com.example.revolut.di.AppComponent
import com.example.revolut.di.AppModule
import com.example.revolut.di.DaggerAppComponent

class RevolutApplication : Application() {

    val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder().appModule(AppModule()).build()
    }
}