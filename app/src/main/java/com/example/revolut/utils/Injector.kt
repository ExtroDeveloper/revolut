package com.example.revolut.utils

import androidx.annotation.VisibleForTesting
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent

/**
 * Helps to inject to fragment and change injection for testing.
 */
class Injector(fragment: Fragment, @VisibleForTesting var inject: () -> Unit) {

    init {
        fragment.lifecycle.addObserver(object : LifecycleObserver {
            @Suppress("unused")
            @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
            fun onCreate() = inject()
        })
    }
}

fun Fragment.withInjection(inject: () -> Unit): Injector {
    return Injector(this, inject)
}