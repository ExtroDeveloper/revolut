package com.example.revolut.utils.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

/**
 * Adapter uses data binging for work with view holders.
 */
open class ViewModelAdapter : RecyclerView.Adapter<ViewHolder>() {

    private val items: MutableList<Any> = ArrayList()

    val cellMap = LinkedHashMap<Class<out Any>, CellInfo>()

    open fun set(newItems: List<Any>) {
        val diffCallback = DiffCallBack(
            items,
            newItems,
            checkAreItemsTheSame = { oldItem: Any, newItem: Any ->
                getCellInfo(oldItem).checkAreItemsTheSame.invoke(oldItem, newItem)
            },
            checkAreContentsTheSame = { oldItem: Any, newItem: Any ->
                getCellInfo(oldItem).checkAreContentsTheSame.invoke(oldItem, newItem)
            })
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        items.clear()
        items.addAll(newItems)
        val value = ListUpdateCallbackImpl(this)
        diffResult.dispatchUpdatesTo(value)
    }

    @JvmOverloads
    inline fun <reified T : Any> cell(
        @LayoutRes layoutId: Int,
        bindingId: Int,
        noinline areItemsTheSame: (T, T) -> Boolean = { a: T, b: T -> a == b },
        noinline areContentsTheSame: (T, T) -> Boolean = { a: T, b: T -> a == b }
    ) {
        @Suppress("UNCHECKED_CAST")
        val cellInfo = CellInfo(
            layoutId,
            bindingId,
            areItemsTheSame as (Any, Any) -> Boolean,
            areContentsTheSame as (Any, Any) -> Boolean
        )
        cellMap[T::class.java] = cellInfo
    }

    //region RecyclerView.Adapter
    override fun getItemCount() = items.size

    override fun getItemViewType(position: Int): Int {
        return getCellInfo(getViewModel(position)).layoutId
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(viewType, parent, false)

        return ViewHolder(view.rootView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val cellInfo = getCellInfo(getViewModel(position))
        onBind(holder.binding, cellInfo, position)
    }
    //endregion

    @Suppress("unused")
    private fun getViewModelType(itemPosition: Int): Class<out Any> {
        return items[itemPosition]::class.java
    }

    private fun onBind(
        binding: ViewDataBinding,
        cellInfo: CellInfo,
        position: Int
    ) {
        val viewModel = getViewModel(position)

        if (cellInfo.bindingId != 0) {
            binding.setVariable(cellInfo.bindingId, viewModel)
            binding.executePendingBindings()
        }
    }

    private fun getViewModel(position: Int) = items[position]

    private fun getCellInfo(viewModel: Any): CellInfo {
        // Find info with simple class check:
        cellMap.entries
            .find { it.key == viewModel.javaClass }
            ?.apply { return value }

        // Find info with inheritance class check:
        cellMap.entries
            .find { it.key.isInstance(viewModel) }
            ?.apply {
                cellMap[viewModel.javaClass] = value
                return value
            }

        val message = "Cell info for class ${viewModel.javaClass.name} not found."
        throw Exception(message)
    }
}