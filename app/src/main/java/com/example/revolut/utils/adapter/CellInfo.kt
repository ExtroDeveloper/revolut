package com.example.revolut.utils.adapter

/**
 * Auxiliary data of adapter item.
 */
data class CellInfo(
    val layoutId: Int,
    val bindingId: Int,
    val checkAreItemsTheSame: (Any, Any) -> Boolean,
    val checkAreContentsTheSame: (Any, Any) -> Boolean
)