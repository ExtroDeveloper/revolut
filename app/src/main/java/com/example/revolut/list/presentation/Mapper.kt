package com.example.revolut.list.presentation

import android.os.Handler
import android.os.Looper
import com.example.revolut.list.domain.CurrencyItems
import com.example.revolut.list.presentation.item.ItemNotifier
import com.example.revolut.list.presentation.item.ItemViewModel
import javax.inject.Inject

/**
 * Maps data from items of domain layer to items of presentation layer.
 */
class Mapper constructor(
    private val subtitleProvider: SubtitleProvider,
    private val handler: Handler
) {
    @Inject
    constructor(subtitleProvider: SubtitleProvider) : this(
        subtitleProvider,
        Handler(Looper.getMainLooper())
    )

    /**
     * Maps item of domain layer to item of presentation layer.
     */
    fun map(currencyItems: CurrencyItems, itemListener: ItemListener): List<ItemViewModel> {
        return currencyItems.currencyItems.map {
            ItemViewModel(
                it.name,
                subtitleProvider.getFullName(it.name),
                String.format("%.2f", it.amount),
                ItemNotifier(
                    it.name,
                    itemListener
                ),
                handler
            )
        }
    }
}