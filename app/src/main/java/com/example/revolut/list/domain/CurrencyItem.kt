package com.example.revolut.list.domain

/**
 * Item of currency list screen.
 */
data class CurrencyItem(val name: String, val rate: Float, val amount: Float)