package com.example.revolut.list.presentation.item

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData

/**
 * Observer of [liveData], notifying [notifier] only when new data is different with old data.
 */
class AmountObserver(
    @VisibleForTesting var previous: String,
    @VisibleForTesting var liveData: LiveData<String>,
    @VisibleForTesting var notifier: ItemNotifier
) : Runnable {

    override fun run() {
        liveData.observeForever {
            if (it != previous) {
                previous = it
                notifier.notifyAmount(it.toFloat())
            }
        }
    }
}