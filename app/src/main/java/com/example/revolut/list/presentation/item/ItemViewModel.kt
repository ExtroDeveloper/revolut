package com.example.revolut.list.presentation.item

import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.lifecycle.MutableLiveData

/**
 * View model of currency list screen item.
 * Uses handler for subscribing to live data.
 */
class ItemViewModel(
    val title: String,
    val subTitle: String,
    initialAmount: String,
    private val notifier: ItemNotifier,
    handler: Handler = Handler(Looper.getMainLooper()),
    val amount: MutableLiveData<String> = MutableLiveData(initialAmount)
) : View.OnClickListener {

    val icon = if (title.isNotEmpty()) title.substring(0, 1) else ""

    init {
        handler.post(AmountObserver(initialAmount, amount, notifier))
    }

    override fun onClick(v: View?) {
        notifier.notifyClick()
    }

    /**
     * Check if represent the same item as [toCompare].
     */
    fun areItemsTheSame(toCompare: ItemViewModel) = this.title == toCompare.title

    /**
     * Check if represent the same content as [toCompare].
     * Everything in the item ignored except [amount] because nothing other doesn't going to change in application.
     */
    fun areContentsTheSame(toCompare: ItemViewModel) = this.amount.value == toCompare.amount.value
}