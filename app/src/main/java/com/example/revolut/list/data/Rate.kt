package com.example.revolut.list.data

/**
 * Name of [currency] with according to it [rate].
 */
data class Rate(val currency: String, val rate: Float)