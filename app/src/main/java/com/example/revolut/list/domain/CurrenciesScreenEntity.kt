package com.example.revolut.list.domain

import com.example.revolut.list.data.Rates
import java.util.*
import javax.inject.Inject

/**
 * Represents state of screen containing a list of currencies and way to manipulate it, for example change order
 * or update with new data.
 * Is passed to the presentation layer, should be updated only by an interactor in the domain layer.
 */
class CurrenciesScreenEntity @Inject constructor() {

    private var baseCurrency: String = ""

    /**
     * List of screen items.
     */
    var items = CurrencyItems(emptyList())
        private set

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CurrenciesScreenEntity

        if (baseCurrency != other.baseCurrency) return false
        if (items != other.items) return false

        return true
    }

    override fun hashCode(): Int {
        var result = baseCurrency.hashCode()
        result = 31 * result + items.hashCode()
        return result
    }

    /**
     * Set new data, but order will be the same for items that were before according to names.
     */
    fun setRates(rates: Rates) {
        val map = TreeMap<String, Int>()
        items.currencyItems.forEachIndexed { index, item ->
            map[item.name] = index
        }

        val items = mutableListOf<CurrencyItem>()
        items.add(CurrencyItem(rates.baseCurrency, 1f, getAmount(rates.baseCurrency, 1f)))
        rates.items
            .forEach {
                items.add(CurrencyItem(it.currency, it.rate, getAmount(it.currency, it.rate)))
            }

        items.sortBy {
            map[it.name]
        }

        this.items = CurrencyItems(items)
    }

    /**
     * Change value of currency with [name] to new [value], values off other items will be recalculated.
     */
    fun update(name: String, value: Float) {
        val item = items.currencyItems.firstOrNull {
            it.name == name
        } ?: return

        val count = value / item.rate

        items = CurrencyItems(items.currencyItems.map {
            CurrencyItem(
                it.name,
                it.rate,
                if (it.name == name) value else it.rate * count
            )
        })
    }

    /**
     * Put item with [name] on top of [items] by shifting others down.
     */
    fun putOnTop(name: String) {
        val items = items.currencyItems.toMutableList()
        items.find { it.name == name }?.let {
            if (items.indexOf(it) != 0) {
                items.remove(it)
                items.add(0, it)
            }
        }
        this.items = CurrencyItems(items)
    }

    private fun getAmount(name: String, rate: Float): Float {
        items.currencyItems.find { it.name == name }?.let {
            return it.amount / it.rate * rate
        }
        return 0f
    }
}