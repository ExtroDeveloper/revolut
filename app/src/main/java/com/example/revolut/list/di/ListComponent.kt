package com.example.revolut.list.di

import com.example.revolut.di.AppComponent
import com.example.revolut.list.presentation.ListViewModelFactory
import dagger.Component

/**
 * List screen DI component.
 */
@Component(dependencies = [AppComponent::class])
interface ListComponent {

    /**
     * Provide factory to create view model containing list of rates.
     */
    fun provideViewModelFactory(): ListViewModelFactory
}