package com.example.revolut.list.domain

import com.example.revolut.list.data.Rates
import com.example.revolut.list.data.RatesRepository
import com.example.revolut.list.presentation.ListViewModel
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import timber.log.Timber
import java.util.concurrent.TimeUnit.SECONDS
import javax.inject.Inject

/**
 * Represents domain logic of application.
 */
class CurrenciesScreenInteractor @Inject constructor(
    private var currenciesScreenEntity: CurrenciesScreenEntity = CurrenciesScreenEntity(),
    private val repository: RatesRepository
) {
    /**
     *  Obtain data from [repository] immediately and show it on [viewModel] and repeat
     *  every [UPDATE_TIME_SEC] seconds.
     */
    fun listenForData(
        viewModel: ListViewModel
    ): Disposable {
        return Observable
            .interval(
                0,
                UPDATE_TIME_SEC,
                SECONDS,
                Schedulers.io()
            )
            .flatMap {
                val data = repository.getData()
                data
            }
            .onErrorReturn {
                Timber.e(it)
                Rates("", emptyList())
            }
            .subscribe {
                currenciesScreenEntity.setRates(it)
                viewModel.show(currenciesScreenEntity)
            }
    }

    /**
     * Change value of currency with [name] to new [value] and show state on [viewModel].
     */
    fun update(
        name: String,
        value: Float,
        viewModel: ListViewModel
    ): Disposable {
        currenciesScreenEntity.update(name, value)
        return listenForData(viewModel)
    }

    /**
     * Put item with [name] on top and show state on [viewModel].
     */
    fun putOnTop(
        name: String,
        viewModel: ListViewModel
    ): Disposable {
        currenciesScreenEntity.putOnTop(name)
        return listenForData(viewModel)
    }
}

const val UPDATE_TIME_SEC = 1L