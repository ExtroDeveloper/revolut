package com.example.revolut.list.data

import com.example.revolut.list.data.Rate

/**
 * Rates data, that is used to update the state of currency list.
 */
class Rates(
    val baseCurrency: String,
    val items: List<Rate> = emptyList()
)