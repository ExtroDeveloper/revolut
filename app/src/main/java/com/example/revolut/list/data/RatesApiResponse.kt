package com.example.revolut.list.data

import com.google.gson.annotations.SerializedName

/**
 * POJO representing cloud storage response.
 */
data class RatesApiResponse(

    @SerializedName("baseCurrency") val baseCurrency: String,
    @SerializedName("rates") val rates: Map<String, Float>
)