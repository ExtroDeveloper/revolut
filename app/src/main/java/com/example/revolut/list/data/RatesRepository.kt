package com.example.revolut.list.data

import javax.inject.Inject

/**
 * Repository provides data for currency list by fetching it from [service].
 */
class RatesRepository @Inject constructor(private val service: Service) {

    fun getData() = service
        .ratesData()
        .map {
            val rates = mutableListOf<Rate>()
            it.rates.forEach { (currency, rate) ->
                rates.add(Rate(currency, rate))
            }

            Rates(it.baseCurrency, rates)
        }!!
}