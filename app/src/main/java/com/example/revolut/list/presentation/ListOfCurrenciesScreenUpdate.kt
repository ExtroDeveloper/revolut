package com.example.revolut.list.presentation

import com.example.revolut.list.presentation.item.ItemViewModel

/**
 * Event of screen update.
 */
data class ListOfCurrenciesScreenUpdate(val items: List<ItemViewModel>, val needScroll: Boolean)