package com.example.revolut.list.domain

/**
 * Keeps [currencyItems] of currency list screen.
 */
data class CurrencyItems(val currencyItems: List<CurrencyItem> = emptyList())