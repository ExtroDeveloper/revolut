package com.example.revolut.list.presentation

import android.view.View
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.revolut.list.domain.CurrenciesScreenEntity
import com.example.revolut.list.domain.CurrenciesScreenInteractor
import com.example.revolut.list.presentation.item.ItemViewModel
import io.reactivex.rxjava3.disposables.SerialDisposable
import javax.inject.Inject

/**
 * View model of currency list screen. Obtains data and call login via [interactor].
 */
class ListViewModel(
    private val interactor: CurrenciesScreenInteractor,
    private val mapper: Mapper,
    private val disposable: SerialDisposable
) : ViewModel(), ItemListener, View.OnScrollChangeListener {

    @Inject
    constructor(
        interactor: CurrenciesScreenInteractor,
        mapper: Mapper
    ) : this(
        interactor,
        mapper,
        SerialDisposable()
    )

    private var needScroll: Boolean = false
    private val _items = MutableLiveData<List<ItemViewModel>>()
    val listOfCurrenciesScreenUpdateEvent: LiveData<ListOfCurrenciesScreenUpdate> = Transformations.map(_items) {
        ListOfCurrenciesScreenUpdate(it, needScroll)
    }

    init {
        disposable.set(interactor.listenForData(this))
    }

    public override fun onCleared() {
        disposable.dispose()
    }

    override fun setAmount(name: String, value: Float) {
        disposable.set(interactor.update(name, value, this))
    }

    override fun onClick(name: String) {
        needScroll = true
        disposable.set(interactor.putOnTop(name, this))
    }

    override fun onScrollChange(v: View?, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int) {
        if (scrollY == 0) needScroll = false
    }

    @WorkerThread
    fun show(currenciesScreenEntity: CurrenciesScreenEntity) {
        _items.postValue(mapper.map(currenciesScreenEntity.items, this))
    }
}