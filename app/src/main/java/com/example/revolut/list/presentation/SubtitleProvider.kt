package com.example.revolut.list.presentation

import javax.inject.Inject

/**
 * Provider of subtitles for currencies.
 */
class SubtitleProvider @Inject constructor() {

    private val map = mutableMapOf<String, String>().also {
        it["EUR"] = "eUD"
        it["AUD"] = "aUD"
        it["BGN"] = "bGN"
        it["BRL"] = "bRL"
        it["CAD"] = "cAD"
        it["CHF"] = "cHF"
        it["CNY"] = "cNY"
        it["CZK"] = "cZK"
        it["DKK"] = "dKK"
        it["GBP"] = "gBP"
        it["HKD"] = "hKD"
        it["HRK"] = "hRK"
        it["HUF"] = "hUF"
        it["IDR"] = "iDR"
        it["ILS"] = "iLS"
        it["INR"] = "iNR"
        it["ISK"] = "iSK"
        it["JPY"] = "jPY"
        it["KRW"] = "kRW"
        it["MXN"] = "mXN"
        it["MYR"] = "mYR"
        it["NOK"] = "nOK"
        it["NZD"] = "nZD"
        it["PHP"] = "pHP"
        it["PLN"] = "pLN"
        it["RON"] = "rON"
        it["RUB"] = "rUB"
        it["SEK"] = "sEK"
        it["SGD"] = "sGD"
        it["THB"] = "tHB"
        it["USD"] = "uSD"
        it["ZAR"] = "zAR"
    }

    /**
     * Provide subtitle for currency.
     */
    fun getFullName(name: String): String {
        return map[name] ?: ""
    }

}
