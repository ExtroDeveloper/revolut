package com.example.revolut.list.presentation.view

import android.os.Bundle
import android.view.View
import androidx.annotation.VisibleForTesting
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.revolut.R
import com.example.revolut.RevolutApplication
import com.example.revolut.databinding.ListOfCurrenciesFragmentBinding
import com.example.revolut.list.di.DaggerListComponent
import com.example.revolut.list.presentation.ListViewModel
import com.example.revolut.list.presentation.withFactory
import com.example.revolut.utils.withInjection

/**
 * Currency list screen.
 */
class ListOfCurrenciesFragment : Fragment(R.layout.list_of_currencies_fragment) {

    @Suppress("unused")
    @VisibleForTesting
    val injector = withInjection {
        viewModel = withFactory(
            DaggerListComponent
                .builder()
                .appComponent((requireContext().applicationContext as RevolutApplication).appComponent)
                .build()
                .provideViewModelFactory(),
            ListViewModel::class.java
        )
    }

    lateinit var viewModel: ListViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        ListOfCurrenciesFragmentBinding.bind(view).also {
            val adapter = ListOfCurrenciesAdapter()
            it.recyclerView.adapter = adapter
            viewModel.listOfCurrenciesScreenUpdateEvent.observe(
                viewLifecycleOwner,
                Observer { update ->
                    adapter.set(update.items)
                    if (update.needScroll) {
                        it.recyclerView.scrollToPosition(0)
                    }
                }
            )
            it.recyclerView.itemAnimator?.changeDuration = 0
            it.recyclerView.setOnScrollChangeListener(viewModel)
            it.executePendingBindings()
        }
    }
}