package com.example.revolut.list.data

import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET

/**
 * Description of a request to the web storage.
 */
interface Service {
    @GET("api/android/latest?base=EUR")
    fun ratesData(): Observable<RatesApiResponse>
}