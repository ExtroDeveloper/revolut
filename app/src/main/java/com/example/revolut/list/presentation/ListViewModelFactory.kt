package com.example.revolut.list.presentation

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject
import javax.inject.Provider

class ListViewModelFactory @Inject constructor(
    private val provider: Provider<ListViewModel>
) : ViewModelProvider.Factory {

    override fun <VIEW_MODEL : ViewModel?> create(modelClass: Class<VIEW_MODEL>): VIEW_MODEL {
        @Suppress("UNCHECKED_CAST")
        return provider.get() as VIEW_MODEL
    }
}

fun <VIEW_MODEL : ViewModel> Fragment.withFactory(
    factory: ViewModelProvider.Factory,
    vmClass: Class<VIEW_MODEL>
): VIEW_MODEL =
    ViewModelProvider(
        this,
        factory
    )[vmClass]