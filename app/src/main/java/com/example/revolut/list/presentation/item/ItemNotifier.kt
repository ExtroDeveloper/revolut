package com.example.revolut.list.presentation.item

import com.example.revolut.list.presentation.ItemListener

/**
 * Notifies [itemListener] about change of value or click for currency with [name].
 */
class ItemNotifier(val name: String, private val itemListener: ItemListener) {

    /**
     * Notify [itemListener] about new [value].
     */
    fun notifyAmount(value: Float) {
        itemListener.setAmount(name, value)
    }

    /**
     * Notify [itemListener] about click on item.
     */
    fun notifyClick() {
        itemListener.onClick(name)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ItemNotifier

        if (name != other.name) return false
        if (itemListener != other.itemListener) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + itemListener.hashCode()
        return result
    }
}