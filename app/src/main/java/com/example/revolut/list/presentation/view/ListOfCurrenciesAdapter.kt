package com.example.revolut.list.presentation.view

import com.example.revolut.BR
import com.example.revolut.R
import com.example.revolut.list.presentation.item.ItemViewModel
import com.example.revolut.utils.adapter.ViewModelAdapter

class ListOfCurrenciesAdapter : ViewModelAdapter() {

    init {
        cell(
            R.layout.list_of_currencies_item,
            BR.viewModel,
            areItemsTheSame = ItemViewModel::areItemsTheSame,
            areContentsTheSame = ItemViewModel::areContentsTheSame
        )
    }
}