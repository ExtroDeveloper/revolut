package com.example.revolut

import androidx.appcompat.app.AppCompatActivity

/**
 * Entry point to the application, contains container to keep list fragment.
 */
class MainActivity : AppCompatActivity(R.layout.main_activity)